function [ normalilzed_features] = normalizeWithMuSigma( input_matrix, mu, sigma)
%NORMALIZEWITHMUSIGMA
%   
for i = 1:size(input_matrix,2)
    normalilzed_features(:,i) = (input_matrix(:,i) - mu(i))./sigma(i);
end
end

