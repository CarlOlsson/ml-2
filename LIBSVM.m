clear all;
close all;
clc;

% Import training set from file
A = csvread('training.csv');
% Split training set into data and labels
TrainLabel = A(:, end);
TrainSet = A(:,1:end - 1);
% Import validation set from file
ValidationSet = csvread('validation.csv');
% Import test set from file
TestSet = csvread('testing.csv');

% Calculate mu and sigma for every feature
[~,mu,sigma] = normalizeAllFeatures([TrainSet;ValidationSet;TestSet]);

% Normalize the sets
TrainTest_normalized = normalizeWithMuSigma(TrainSet,mu,sigma);
ValidationSet_normalized = normalizeWithMuSigma(ValidationSet,mu,sigma);
TestTest_normalized = normalizeWithMuSigma(TestSet,mu,sigma);

% Perform grid search for cost and gamma
folds = 5; %5-fold cross validation
[C,gamma] = meshgrid(-5:3:15, -15:3:3); %grid of parameters

cv_acc = zeros(numel(C),1); 
d = 2; %Radial Basis Functions as Kernel

% Different cost for false positive
fpc = 5;

for i=1:numel(C)   
    cv_acc(i) = svmtrain(TrainLabel,TrainTest_normalized, ...          
        sprintf('-c %f -g %f -v %d -t %d -w1 %d', 2^C(i), 2^gamma(i), folds,d,fpc));
end
%# pair (C,gamma) with best accuracy
[~,idx] = max(cv_acc); 
% Calculate the best C and gamma
best_C = 2^C(idx); best_gamma = 2^gamma(idx);
%% surf (or contour) plot of paramter selection 
% contour(C, gamma, reshape(cv_acc,size(C))), colorbar
surf(C, gamma, reshape(cv_acc,size(C))), colorbar
hold on;
text(C(idx), gamma(idx), sprintf('Acc = %.2f %%',cv_acc(idx)), ...  
    'HorizontalAlign','left', 'VerticalAlign','top') 
hold off 
xlabel('log_2(C)'), ylabel('log_2(\gamma)'), title('Cross-Validation Accuracy') 

%% Train the SVM
svmModel=svmtrain(TrainLabel,TrainTest_normalized,['-c ' num2str(best_C) ' -g ' num2str(best_gamma) ' -t ' num2str(d) ' -w1 5']);

%   Dummy Vector of -1 and 1 (Since we don't have access to the validation
%   set labels
Dummy = randn(size(A, 1), 1);
Dummy(Dummy <= 0) = -1;
Dummy(Dummy > 0) = 1;

%% Predict labels on the validation set
svmLabels = svmpredict(Dummy, ValidationSet_normalized, svmModel);
csvwrite('nameOfFile.csv', svmLabels);


