%   Import training set from file
A = csvread('training.csv');

%   Split training set into data and labels
Training = A(:,1:end - 1);
Group = A(:, end);

%   Cost Matrix
Cost = ...
    [0 1;
    1 0];    

%   Calculate SVM model
svmData = fitcsvm(Training, Group, ...
    'KernelFunction', 'rbf', ...
    'Cost', Cost, ...
    'ScoreTransform', 'sign');

%   Import validation set
ValidationSet = csvread('validation.csv');

%   Predict Validation labels using SVM model and Validation set
svmLabels = predict(svmData, ValidationSet);