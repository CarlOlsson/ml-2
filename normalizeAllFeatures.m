function [ normalilzed_features, mu, sigma ] = normalizeAllFeatures( input_matrix )
%NORMALIZEALLFEATURES 
%   
mu=mean(input_matrix);
sigma=std(input_matrix);
for i = 1:size(input_matrix,2)
    normalilzed_features(:,i) = (input_matrix(:,i) - mu(i))./sigma(i);
end
end

